<?php

declare(strict_types=1);

namespace Drupal\fox_generator\Drush\Generators;

use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\fox\FoxCommandsHelperTrait;
use DrupalCodeGenerator\Asset\AssetCollection as Assets;
use DrupalCodeGenerator\Attribute\Generator;
use DrupalCodeGenerator\Command\BaseGenerator;
use DrupalCodeGenerator\GeneratorType;
use DrupalCodeGenerator\Validator\Required;

/**
 * Fox command generator.
 */
#[Generator(
  name: 'fox:command',
  description: 'Generate Fox command',
  templatePath: __DIR__,
  type: GeneratorType::OTHER,
)]

/**
 * Generator class for Fox.
 */
final class FoxGenerator extends BaseGenerator {

  use FoxCommandsHelperTrait;
  use StringTranslationTrait;

  /**
   * {@inheritdoc}
   */
  protected function generate(array &$vars, Assets $assets): void {
    $ir = $this->createInterviewer($vars);

    $vars['command_name'] = $ir->ask('Fox Command name', validator: new Required());

    // Checking for existing Fox command.
    $commands = [];
    $foxCommands = $this->foxCommandsHelper()
      ->foxCommandsManager()
      ->getDefinitions();
    foreach ($foxCommands as $name => $info) {
      $commands[] = strtoupper($name);
    }

    if (in_array(strtoupper($vars['command_name']), $commands)) {
      $this->io()->error((string) $this->t('The command "@command" has already existed', [
        '@command' => $vars['command_name'],
      ]));
      return;
    }

    $vars['command_description'] = $ir->ask('Fox Command description', validator: new Required());
    $vars['class'] = $ir->askClass(default: 'FoxCommand{command_name|camelize}');

    $foxModulePath = \Drupal::service('module_handler')
      ->getModule('fox')
      ->getPath();

    $root = str_replace($this->io()->getWorkingDirectory() . '/', '', DRUPAL_ROOT);
    $assets->addFile($root . '/' . $foxModulePath . '/src/Plugin/FoxCommand/{class}.php', 'fox-generate.twig');

    $this->io()->info((string) $this->t('New Fox command @command has been created', [
      '@command' => $vars['command_name'],
    ]));
  }

}
