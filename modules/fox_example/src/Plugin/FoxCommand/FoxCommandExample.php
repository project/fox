<?php

namespace Drupal\fox_example\Plugin\FoxCommand;

use Drupal\fox\Plugin\FoxCommand\FoxCommandBaseClass;

/**
 * EXAMPLE fox command.
 *
 * @FoxCommand(
 *   id = "example",
 *   label = @Translation("Get entity count. Usage: EXAMPLE {entity_type}")
 * )
 */
class FoxCommandExample extends FoxCommandBaseClass {

  /**
   * {@inheritdoc}
   */
  public function execute(array $params, array $variables): array {
    if (empty($params)) {
      return $this->errorReturn($this->t('Usage: EXAMPLE {entity_type}'));
    }

    $entity_type = $params[0];
    $helper = $this->foxCommandsHelper();

    try {
      $query = $helper->getEntityQuery($entity_type);
      $ids = $query->execute();
    }
    catch (\Exception $e) {
      return $this->errorReturn($e->getMessage());
    }

    $entity_count = count($ids);

    return [
      'message' => $this->t('Count items for entity type "@entity_type": @count', [
        '@entity_type' => $entity_type,
        '@count' => $entity_count,
      ]),
      'variables' => [
        'example_entity_count' => $entity_count,
      ],
    ];
  }

}
