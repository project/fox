# Fox examplw module

The module improves example for creating custom Fox command.
The EXAMPLE command returns users count.

Usage: example

## Table of contents

- Requirements
- Installation
- Configuration
- Maintainers

## Requirements

  - Install fox module

## Installation

Install as you would normally install a contributed Drupal module. For further information, see [Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).

## Configuration

There is no configuration.

## Maintainers

- Sergey Loginov - [goodboy](https://drupal.org/user/222910)
