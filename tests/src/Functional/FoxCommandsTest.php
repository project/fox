<?php

declare(strict_types=1);

namespace Drupal\Tests\fox\Functional;

use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Tests\BrowserTestBase;
use Drush\TestTraits\DrushTestTrait;

/**
 * Test description.
 *
 * @group fox
 */
final class FoxCommandsTest extends BrowserTestBase {

  use DrushTestTrait;
  use StringTranslationTrait;

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'claro';

  /**
   * {@inheritdoc}
   */
  protected static $modules = ['fox', 'node'];

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $this->drupalCreateContentType([
      'type' => 'fox',
      'name' => 'Fox page',
    ]);
  }

  /**
   * Test Fox commands.
   */
  public function testFoxCommand(): void {
    $welcome_text = $this->t('[INFO] Welcome to the Fox console!');
    $quit_text = $this->t('[INFO] Exiting the Fox console.');

    $commands = [
      'USE node.fox',
      'APPEND title WITH record1',
      'APPEND title WITH record2',
      'APPEND title WITH record3',
      'APPEND title WITH record4',
      'GO TOP',
      'GO NEXT',
      'GO PREV',
      'GO BOTTOM',
      'SET r TO 2',
      'GO @r',
      'IF @r > 1 THEN GO 3',
      'GO TOP',
      'REPLACE title WITH "alter record1"',
      'DELETE',
      'SET r TO',
      'SELECT uuid FROM user ORDER BY uid INTO data',
      'SELECT uid WHERE uuid = "@data.0.uuid"',
      'QUIT',
    ];
    $input = implode(';', $commands);
    $this->drush('fox', [], ['input' => $input]);

    $messages = [
      $welcome_text . PHP_EOL,
      'Entity type and bundle were changed',
      'entity_type="node" bundle="fox" count=0',
      'Appended',
      'entity_type="node" bundle="fox" count=1 id=1',
      'Appended',
      'entity_type="node" bundle="fox" count=2 id=2',
      'Appended',
      'entity_type="node" bundle="fox" count=3 id=3',
      'Appended',
      'entity_type="node" bundle="fox" count=4 id=4',
      'Set record to 1',
      'entity_type="node" bundle="fox" count=4 id=1 recno=1',
      'Set record to 2',
      'entity_type="node" bundle="fox" count=4 id=2 recno=2',
      'Set record to 1',
      'entity_type="node" bundle="fox" count=4 id=1 recno=1',
      'Set record to 4',
      'entity_type="node" bundle="fox" count=4 id=4 recno=4',
      'Variable r was set',
      'entity_type="node" bundle="fox" count=4 id=4 recno=4 r=2',
      'Set record to 2',
      'entity_type="node" bundle="fox" count=4 id=2 recno=2 r=2',
      'Set record to 3',
      'entity_type="node" bundle="fox" count=4 id=3 recno=3 r=2',
      'Set record to 1',
      'entity_type="node" bundle="fox" count=4 id=1 recno=1 r=2',
      'Replaced',
      'entity_type="node" bundle="fox" count=4 id=1 recno=1 r=2',
      'Deleted',
      'entity_type="node" bundle="fox" count=3 r=2',
      'Variable r was deleted',
      'entity_type="node" bundle="fox" count=3',
      'entity_type="user" id=0 recno=1',
      '-----',
      'Uid',
      '-----',
      '0',
      '-----',
      '',
      'entity_type="user" id=0 recno=1',
      $quit_text,
    ];
    $text = implode(PHP_EOL, $messages);
    $this->assertOutputEquals($text);
  }

}
