<?php

namespace Drupal\fox;

/**
 * Provides a trait for the common functions.
 */
trait FoxCommonFunctionsTrait {

  /**
   * Error return.
   *
   * @param mixed $message
   *   Message text.
   *
   * @return array
   *   Return error data.
   */
  public function errorReturn($message): array {
    return ['error' => (string) $message];
  }

}
