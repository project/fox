<?php

namespace Drupal\fox;

/**
 * Provides a trait for the Fox commands helper service.
 */
trait FoxCommandsHelperTrait {

  /**
   * The Fox commands helper service.
   *
   * @var \Drupal\fox\FoxCommandsHelper
   */
  protected $foxCommandsHelper;

  /**
   * Gets the Fox commands helper service.
   *
   * @return \Drupal\fox\FoxCommandsHelper
   *   The Fox commands helper service.
   */
  public function foxCommandsHelper() {
    if (!isset($this->foxCommandsHelper)) {
      $this->foxCommandsHelper = \Drupal::service('fox.commands.helper');
    }
    return $this->foxCommandsHelper;
  }

}
