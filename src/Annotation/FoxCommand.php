<?php

namespace Drupal\fox\Annotation;

use Drupal\Component\Annotation\Plugin;

/**
 * Defines a FoxCommand annotation object.
 *
 * @Annotation
 */
class FoxCommand extends Plugin {

  /**
   * The plugin ID.
   *
   * @var string
   */
  public $id;

  /**
   * The plugin label.
   *
   * @var string
   */
  public $label;

}
