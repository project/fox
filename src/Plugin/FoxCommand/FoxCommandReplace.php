<?php

namespace Drupal\fox\Plugin\FoxCommand;

use Drupal\Component\Serialization\Json;

/**
 * REPLACE fox command.
 *
 * @FoxCommand(
 *   id = "replace",
 *   label = @Translation("Replace fields for context record. Usage: REPLACE uField1 WITH eExpr1 [, uField2 WITH eExpr2]")
 * )
 */
class FoxCommandReplace extends FoxCommandBaseClass {

  /**
   * {@inheritdoc}
   */
  public function execute(array $params, array $variables): array {
    if (empty($params)) {
      return $this->errorReturn($this->t('Empty params'));
    }

    $helper = $this->foxCommandsHelper();

    $entity = $helper->getEntity($variables);
    if (is_array($entity) and isset($entity['error'])) {
      return $entity;
    }
    if (empty($entity)) {
      return $this->errorReturn($this->t('Empty entity'));
    }

    $items = $helper->prepareParameters($params);
    foreach ($items as $item) {
      $value = explode(' ', trim($item));
      if (count($value) < 3) {
        return $this->errorReturn($this->t('Bad REPLACE format'));
      }
      $field = array_shift($value);
      $with = array_shift($value);

      $with = strtolower($with);
      if ($with !== 'with') {
        return $this->errorReturn($this->t('There is no WITH part'));
      }

      try {
        $value = $helper->prepareValue($value);
        $replace = Json::decode($value) ?? $value;
        $replace = $helper->stringRender($replace, $variables);

        $entity->set($field, $replace);
        $entity->save();
      }
      catch (\Exception $e) {
        return $this->errorReturn($e->getMessage());
      }
    }

    return [
      'message' => $this->t('Replaced'),
    ];
  }

}
