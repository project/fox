<?php

namespace Drupal\fox\Plugin\FoxCommand;

use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\fox\FoxCommandsHelperTrait;
use Drupal\fox\FoxCommonFunctionsTrait;
use Drupal\fox\Plugin\FoxCommandInterface;

/**
 * FoxCommandBase plugin.
 */
class FoxCommandBaseClass implements FoxCommandInterface {

  use StringTranslationTrait;
  use FoxCommandsHelperTrait;
  use FoxCommonFunctionsTrait;

  /**
   * {@inheritdoc}
   */
  public function getPluginId() {
    // Return the unique identifier for the plugin.
    return $this->pluginId;
  }

  /**
   * {@inheritdoc}
   */
  public function getPluginDefinition() {
    // Return the plugin definition.
    return [
      'id' => $this->pluginId,
      'label' => $this->label,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function execute(array $params, array $variables): array {
    return [];
  }

}
