<?php

namespace Drupal\fox\Plugin\FoxCommand;

/**
 * GO fox command.
 *
 * @FoxCommand(
 *   id = "go",
 *   label = @Translation("Set context record. Usage: GO {TOP|BOTTOM|PREV [step]|NEXT [step]|number}")
 * )
 */
class FoxCommandGo extends FoxCommandBaseClass {

  const TOP = 'TOP';
  const BOTTOM = 'BOTTOM';
  const PREV = 'PREV';
  const NEXT = 'NEXT';

  /**
   * {@inheritdoc}
   */
  public function execute(array $params, array $variables): array {
    if (empty($params)) {
      return $this->errorReturn($this->t('Empty parameter. Usage: GO {TOP|BOTTOM|PREV [step]|NEXT [step]|number}'));
    }
    $direction = reset($params);

    $helper = $this->foxCommandsHelper();

    $entity_type = $variables['entity_type'] ?? NULL;
    if (empty($entity_type)) {
      return $this->errorReturn($this->t('Empty entity type. Use USE command.'));
    }
    $bundle = $variables['bundle'] ?? NULL;

    try {
      $query = $helper->getEntityQuery($entity_type, $bundle);

      $id_key = $helper->entityKey($entity_type, 'id');
      $query->sort($id_key);

      $direction = $helper->stringRender($direction, $variables);
      if (is_numeric($direction)) {
        $recno = $direction;
        $query->range($direction - 1, 1);
      }
      else {
        $directions = [self::TOP, self::BOTTOM, self::PREV, self::NEXT];
        $direction = strtoupper($direction);
        if (!in_array($direction, $directions)) {
          return $this->errorReturn($this->t('Wrong GO parameter: @direction', [
            '@direction' => $direction,
          ]));
        }

        if (in_array($direction, [self::PREV, self::NEXT])) {
          $recno = $variables['recno'] ?? NULL;
          if ($recno === NULL) {
            return $this->errorReturn($this->t('Empty current record number'));
          }

          $step = $params[1] ?? 1;
          if (!is_numeric($step)) {
            return $this->errorReturn($this->t('Step parameter @step is not numeric', [
              '@step' => $step,
            ]));
          }

          if ($direction === self::PREV) {
            $step *= -1;
          }

          $recno += $step;
          $query->range($recno - 1, 1);
        }
        else {
          $sort = ($direction === self::BOTTOM) ? 'DESC' : 'ASC';
          $query->sort($id_key, $sort);
          $query->range(0, 1);

          $recno = ($direction === self::BOTTOM) ? $variables['count'] : 1;
        }
      }

      $ids = $query->execute();
    }
    catch (\Exception $e) {
      return $this->errorReturn($this->t('Wrong parameter'));
    }

    if (empty($ids)) {
      return $this->errorReturn($this->t('Empty data'));
    }

    $id = reset($ids);
    return [
      'message' => dt('Set record to @recno', [
        '@recno' => $recno,
      ]),
      'variables' => [
        'id' => $id,
        'recno' => $recno,
      ],
    ];
  }

}
