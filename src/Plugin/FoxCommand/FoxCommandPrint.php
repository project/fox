<?php

namespace Drupal\fox\Plugin\FoxCommand;

/**
 * PRINT fox command.
 *
 * @FoxCommand(
 *   id = "print",
 *   label = @Translation("Print variable. Usage: PRINT {variable1,variable2,...}")
 * )
 */
class FoxCommandPrint extends FoxCommandBaseClass {

  /**
   * {@inheritdoc}
   */
  public function execute(array $params, array $variables): array {
    if (empty($params)) {
      return $this->errorReturn($this->t('Empty command parameter.'));
    }

    $helper = $this->foxCommandsHelper();

    $param = reset($params);
    $message = [];
    $vars = explode(',', $param);

    foreach ($vars as $var) {
      $value = $helper->stringRender($var, $variables);
      if (is_array($value) || is_object($value)) {
        $value = print_r($value, TRUE);
      }

      $message[] = ($value === $var) ?
        (string) $this->t('Variable @var does not find', ['@var' => $var]) : $value;
    }

    return [
      'message' => $message,
      'hide_variables' => TRUE,
    ];
  }

}
