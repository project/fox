<?php

namespace Drupal\fox\Plugin\FoxCommand;

/**
 * LIST fox command.
 *
 * @FoxCommand(
 *   id = "list",
 *   label = @Translation("List of the Fox commands. Usage: LIST [all|commands|sql]")
 * )
 */
class FoxCommandList extends FoxCommandBaseClass {

  /**
   * {@inheritdoc}
   */
  public function execute(array $params, array $variables): array {
    $type = 'all';
    if (!empty($params)) {
      $type = reset($params);
    }

    $message = [];

    $header = [$this->t('Command'), $this->t('Description')];
    $data = [];

    if (in_array($type, ['all', 'commands'])) {
      $foxCommands = $this->foxCommandsHelper()
        ->foxCommandsManager()
        ->getDefinitions();

      foreach ($foxCommands as $name => $info) {
        $data[$name] = [strtoupper($name), $info['label']];
      }
      ksort($data);

      $message[(string) $this->t('Commands')] = [
        'header' => $header,
        'data' => $data,
      ];
    }

    if (in_array($type, ['all', 'sql'])) {
      $header = [$this->t('Query clauses'), $this->t('Description')];
      $data = [];
      $data[] = ['[SELECT fields]', $this->t('Set fields')];
      $data[] = [
        '[FROM entity_type[.bundle]',
        $this->t('Set entity type, bundle(optional)'),
      ];
      $data[] = ['[WHERE conditions]', $this->t('Filtering query')];
      $data[] = ['[ORDER BY orders]', $this->t('Sorting query')];
      $data[] = ['
        [LIMIT limit OFFSET offset]',
        $this->t('Limit and offset query'),
      ];

      $message[(string) $this->t('Entity query')] = [
        'header' => $header,
        'data' => $data,
      ];
    }

    return [
      'message' => $message,
    ];
  }

}
