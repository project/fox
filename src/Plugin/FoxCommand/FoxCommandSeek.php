<?php

namespace Drupal\fox\Plugin\FoxCommand;

/**
 * SEEK fox command.
 *
 * @FoxCommand(
 *   id = "seek",
 *   label = @Translation("Seek context record. Usage: SEEK id")
 * )
 */
class FoxCommandSeek extends FoxCommandBaseClass {

  /**
   * {@inheritdoc}
   */
  public function execute(array $params, array $variables): array {
    if (empty($params)) {
      return $this->errorReturn($this->t('Empty parameter. Usage: SEEK id'));
    }
    $helper = $this->foxCommandsHelper();

    $id = reset($params);
    $id = $helper->stringRender($id, $variables);
    $variables['id'] = $id;

    $entity = $helper->getEntity($variables);
    if (is_array($entity) and isset($entity['error'])) {
      return $entity;
    }

    if (empty($entity)) {
      return [
        'error' => $this->t('ID @id not found', [
          '@id' => $id,
        ]),
        'variables' => [
          'id' => NULL,
          'recno' => NULL,
        ],
      ];
    }
    else {
      $recno = $helper->getRecnoById($id, $variables);
      return [
        'message' => $this->t('Found'),
        'variables' => [
          'id' => $id,
          'recno' => $recno,
        ],
      ];
    }
  }

}
