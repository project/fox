<?php

namespace Drupal\fox\Plugin\FoxCommand;

use Drupal\Component\Serialization\Json;

/**
 * BROWSE fox command.
 *
 * @FoxCommand(
 *   id = "browse",
 *   label = @Translation("Browse fields for context record. Usage: BROWSE [*|fields] --horizontal=0|[1], --json=[0]|1")
 * )
 */
class FoxCommandBrowse extends FoxCommandBaseClass {

  /**
   * {@inheritdoc}
   */
  public function execute(array $params, array $variables): array {
    $helper = $this->foxCommandsHelper();

    $default_options = [
      'horizontal' => 1,
      'json' => 0,
    ];
    $options_data = $helper->getOptions($params, $default_options);
    $options = $options_data['options'];
    $params = $options_data['params'];

    $entity = $helper->getEntity($variables);
    if (is_array($entity) and isset($entity['error'])) {
      return $entity;
    }
    if (empty($entity)) {
      return $this->errorReturn($this->t('Empty entity'));
    }

    if (method_exists($entity, 'getFields')) {
      $fieldsList = $entity->getFields();
    }
    else {
      $fieldsList = $entity
        ->getEntityType()
        ->getPropertiesToExport();
    }

    if (empty($params) || (current($params) === '*')) {
      $fields = array_keys($fieldsList);
    }
    else {
      $fields = $helper->prepareParameters($params);
    }

    $data = $row = $header = [];
    foreach ($fields as $field) {
      $name = trim($field);
      if ((method_exists($entity, 'hasField') && !$entity->hasField($name))) {
        return $this->errorReturn($this->t('Wrong field "@name"', [
          '@name' => $name,
        ]));
      }

      $value = $value_string = $entity->get($name);
      if (is_object($value)) {
        $value_string = $value->getString();
        $value = $value->getValue();
      }
      elseif (is_array($value)) {
        $value_string = Json::encode($value);
      }

      $json_format = (bool) $options['json'];
      $row[] = $json_format ? Json::encode($value) : $value_string;

      if (isset($fieldsList) && is_object($fieldsList[$name])) {
        $definition = $fieldsList[$name]->getFieldDefinition();
        $header[] = $definition->getLabel() . ' (' . $definition->getName() . ')';
      }
      else {
        $header[] = $name;
      }
    }
    $data[] = $row;

    return [
      'message' => [
        'header' => $header,
        'data' => $data,
        'horizontal' => (bool) $options['horizontal'],
      ],
    ];
  }

}
