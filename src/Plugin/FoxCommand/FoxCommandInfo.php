<?php

namespace Drupal\fox\Plugin\FoxCommand;

/**
 * INFO fox command.
 *
 * @FoxCommand(
 *   id = "info",
 *   label = @Translation("List entities, bundles or fields list for entity, bundle. Usage: INFO [entity_id [bundle]]")
 * )
 */
class FoxCommandInfo extends FoxCommandBaseClass {

  /**
   * {@inheritdoc}
   */
  public function execute(array $params, array $variables): array {
    $helper = $this->foxCommandsHelper();
    $all_entity_types = $helper->entityTypeManager()->getDefinitions();

    if (!empty($params)) {
      $entity_id = $params[0];
      $entity = $all_entity_types[$entity_id] ?? NULL;
      if (!$entity) {
        return $this->errorReturn($this->t('Wrong entity type "@entity_id"', [
          '@entity_id' => $entity_id,
        ]));
      }

      $group = $entity->getGroup();
      if ($this->isContentGroup($group)) {
        return $this->fieldDefinitions($params);
      }
      else {
        $header = [$this->t('ID')];
        $data = [];
        $info = $entity->getPropertiesToExport();
        foreach ($info as $field) {
          $data[] = [$field];
        }

        $message[(string) $this->t('IDs')] = [
          'header' => $header,
          'data' => $data,
        ];

        return [
          'message' => $message,
        ];
      }
    }

    $message = [];
    $header = [$this->t('ID'), $this->t('Title'), $this->t('Bundles')];
    $entity_type_bundle_Info = $helper->entityTypeBundleInfo();
    foreach ($all_entity_types as $entity) {
      $group = $entity->getGroup();
      $id = $entity->id();
      $label = (string) $entity->getLabel();

      $bundle_data = '';
      $bundle_info = $entity_type_bundle_Info->getBundleInfo($id);
      if (!empty($bundle_info)) {
        foreach ($bundle_info as $bundle_id => $bundle) {
          $bundle_label = (string) $bundle['label'];
          $bundle_data = sprintf('%s(%s)', $bundle_id, $bundle_label);
          $message[ucfirst($group)]['data'][] = $this->getGroupData($group, $id, $label, $bundle_data);
        }
      }
      else {
        $message[ucfirst($group)]['data'][] = $this->getGroupData($group, $id, $label);
      }

      if (!isset($message[$group]['header'])) {
        $message[ucfirst($group)]['header'] = $this->getGroupHeader($group);
      }
    }

    foreach ($message as $group_id => $group) {
      $data = $group['data'];
      array_multisort(array_column($data, 'id'), SORT_ASC, $data);
      $message[$group_id]['data'] = $data;
    }

    return [
      'message' => $message,
    ];
  }

  /**
   * Get field definitions.
   *
   * @param array $params
   *   Entity type and bundle.
   *
   * @return array
   *   Fields definitions.
   */
  protected function fieldDefinitions(array $params): array {
    $entity_id = $params[0];
    $bundle = $params[1] ?? $entity_id;

    try {
      $definitions = $this->foxCommandsHelper()
        ->entityFieldManager()
        ->getFieldDefinitions($entity_id, $bundle);
    }
    catch (\Exception $e) {
      return [
        'error' => $e->getMessage(),
      ];
    }

    $header = [$this->t('ID'), $this->t('Name')];
    $data = [];
    foreach ($definitions as $field) {
      $data[] = [$field->getName(), $field->getLabel()];
    }

    $message[(string) $this->t('Fields')] = [
      'header' => $header,
      'data' => $data,
    ];

    return [
      'message' => $message,
    ];
  }

  /**
   * Get group header.
   *
   * @param string $group
   *   Group ID.
   *
   * @return array
   *   Data array.
   */
  protected function getGroupHeader($group) {
    $header = [$this->t('ID'), $this->t('Title')];
    if ($this->isContentGroup($group)) {
      $header[] = $this->t('Bundles');
    }
    return $header;
  }

  /**
   * Get group data.
   *
   * @param string $group
   *   Group ID.
   * @param string $id
   *   Entity ID.
   * @param string $label
   *   Entity label.
   * @param string $bundle_data
   *   Bundle data.
   *
   * @return array
   *   Data array.
   */
  protected function getGroupData($group, $id, $label, $bundle_data = '') {
    $data = ['id' => $id, $label];
    if ($this->isContentGroup($group)) {
      $data[] = $bundle_data;
    }
    return $data;
  }

  /**
   * Is group content type.
   *
   * @param string $group
   *   Group ID.
   *
   * @return bool
   *   The group is content.
   */
  private function isContentGroup($group) {
    return $group === 'content';
  }

}
