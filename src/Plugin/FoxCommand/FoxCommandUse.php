<?php

namespace Drupal\fox\Plugin\FoxCommand;

/**
 * USE fox command.
 *
 * @FoxCommand(
 *   id = "use",
 *   label = @Translation("Change context. USE [context]")
 * )
 */
class FoxCommandUse extends FoxCommandBaseClass {

  /**
   * {@inheritdoc}
   */
  public function execute(array $params, array $variables): array {
    $param = reset($params);

    if (empty($param)) {
      // Clear context.
      return [
        'message' => $this->t('Entity type and bundle were reset'),
        'variables' => [
          'entity_type' => NULL,
          'bundle' => NULL,
          'count' => NULL,
          'id' => NULL,
          'recno' => NULL,
        ],
      ];
    }

    $helper = $this->foxCommandsHelper();

    $parts = explode('.', $param);
    [$entity_type, $bundle] = $helper->getTypeBundle($parts);

    try {
      $query = $helper->getEntityQuery($entity_type, $bundle);
      $ids = $query->execute();
    }
    catch (\Exception $e) {
      return $this->errorReturn($e->getMessage());
    }

    return [
      'message' => $this->t('Entity type and bundle were changed'),
      'variables' => [
        'entity_type' => $entity_type,
        'bundle' => $bundle,
        'count' => count($ids),
        'id' => NULL,
        'recno' => NULL,
      ],
    ];
  }

}
