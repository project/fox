<?php

namespace Drupal\fox\Plugin\FoxCommand;

/**
 * DRUSH fox command.
 *
 * @FoxCommand(
 *   id = "drush",
 *   label = @Translation("Call an external Drush command. Usage: DRUSH command [JSON args] [JSON options]")
 * )
 */
class FoxCommandDrush extends FoxCommandBaseClass {

  /**
   * {@inheritdoc}
   */
  public function execute(array $params, array $variables): array {
    if (empty($params)) {
      return $this->errorReturn($this->t('Empty parameters. Usage: DRUSH command [JSON args] [JSON options].'));
    }

    // Drush command, arguments, options.
    $drush_command = $params[0];

    $drush_command_args = [];
    if (!empty($params[1])) {
      $drush_command_args = json_decode($params[1], TRUE) ?? [];
    }

    $drush_command_options = [];
    if (!empty($params[2])) {
      $drush_command_options = json_decode($params[2], TRUE) ?? [];
    }

    // Get process manager and self variables.
    $process_manager = $variables['process_manager'] ?? NULL;
    $self = $variables['self'] ?? NULL;
    if (!$process_manager || !$self) {
      return $this->errorReturn($this->t('Drush command cannot be executed'));
    }

    try {
      $process = $process_manager->drush($self, $drush_command, $drush_command_args, $drush_command_options);
      $result = $process->mustRun();
    }
    catch (\Exception $e) {
      return $this->errorReturn($e->getMessage());
    }

    if (!$result->isSuccessful()) {
      return $this->errorReturn($this->t('Error command execution'));
    }

    $message = $process->getOutput() ?:
      $this->t('Drush command "@command" was executed successfully', [
        '@command' => $drush_command,
      ]);

    return [
      'message' => $message,
    ];
  }

}
