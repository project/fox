<?php

namespace Drupal\fox\Plugin\FoxCommand;

/**
 * SET fox command.
 *
 * @FoxCommand(
 *   id = "set",
 *   label = @Translation("Set variables. Usage: SET var1 TO eExpr1 [, var2 TO eExpr2]")
 * )
 */
class FoxCommandSet extends FoxCommandBaseClass {

  /**
   * {@inheritdoc}
   */
  public function execute(array $params, array $variables): array {
    if (empty($params)) {
      return $this->errorReturn($this->t('Empty SET variables list'));
    }

    $new_variables = $messages = [];
    $helper = $this->foxCommandsHelper();
    $items = $helper->prepareParameters($params);
    foreach ($items as $item) {
      $value = explode(' ', trim($item));
      if (count($value) < 2) {
        return $this->errorReturn($this->t('Bad SET format'));
      }

      $name = array_shift($value);
      $to = array_shift($value);

      $to = strtolower($to);
      if ($to !== 'to') {
        return $this->errorReturn($this->t('There is no TO part'));
      }

      if ($value === []) {
        $value = NULL;
      }
      else {
        $value = $helper->prepareValue($value);
        $value = $helper->stringRender($value, $variables);

        if (isset($variables[$value])) {
          $value = $variables[$value];
        }
      }

      $new_variables[] = [$name => $value];
      $param = ['@var' => $name];
      $message = is_null($value) ?
        $this->t('Variable @var was deleted', $param) : $this->t('Variable @var was set', $param);
      $messages[] = (string) $message;
    }

    return [
      'message' => $messages,
      'variables' => $new_variables,
    ];
  }

}
