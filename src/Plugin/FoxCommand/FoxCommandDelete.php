<?php

namespace Drupal\fox\Plugin\FoxCommand;

/**
 * DELETE fox command.
 *
 * @FoxCommand(
 *   id = "delete",
 *   label = @Translation("Delete context record. Usage: DELETE")
 * )
 */
class FoxCommandDelete extends FoxCommandBaseClass {

  /**
   * {@inheritdoc}
   */
  public function execute(array $params, array $variables): array {
    $helper = $this->foxCommandsHelper();

    $entity = $helper->getEntity($variables);
    if (is_array($entity) and isset($entity['error'])) {
      return $entity;
    }
    if (empty($entity)) {
      return $this->errorReturn($this->t('Empty entity'));
    }

    try {
      $entity->delete();
    }
    catch (\Exception $e) {
      return $this->errorReturn($e->getMessage());
    }

    return [
      'message' => $this->t('Deleted'),
      'variables' => [
        'id' => NULL,
        'recno' => NULL,
        'count' => $variables['count'] - 1,
      ],
    ];
  }

}
