<?php

namespace Drupal\fox\Plugin;

use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Plugin\DefaultPluginManager;

/**
 * FoxCommands plugin manager.
 */
class FoxCommandsManager extends DefaultPluginManager {

  /**
   * Constructs a new FoxCommandsManager object.
   *
   * @param \Traversable $namespaces
   *   An object that implements \Traversable which contains the root paths
   *   keyed by the corresponding namespace to look for plugin implementations.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache_backend
   *   Cache backend instance to use.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler.
   */
  public function __construct(\Traversable $namespaces, CacheBackendInterface $cache_backend, ModuleHandlerInterface $module_handler) {
    parent::__construct('Plugin/FoxCommand', $namespaces, $module_handler, 'Drupal\fox\Plugin\FoxCommandInterface', 'Drupal\fox\Annotation\FoxCommand');
    $this->alterInfo('fox_commands_info');
    $this->setCacheBackend($cache_backend, 'fox_commands');
  }

}
