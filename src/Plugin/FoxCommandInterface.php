<?php

namespace Drupal\fox\Plugin;

use Drupal\Component\Plugin\PluginInspectionInterface;

/**
 * Defines an interface for FoxCommands plugins.
 */
interface FoxCommandInterface extends PluginInspectionInterface {

  /**
   * Executes the FoxCommand.
   *
   * @param array $params
   *   Parameters of the command.
   * @param array $variables
   *   Current variables.
   *
   * @return array
   *   The result of the FoxCommand execution.
   */
  public function execute(array $params, array $variables): array;

}
