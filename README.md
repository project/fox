# Fox module

The module improves drush infinitive console (like mysql). Use query with
simplified SQL syntax or Foxpro-like commands.

Example 1a: Show title for record #2 for page content type
  USE node.page
  GOTO 2
  BROWSE title, nid

Example 1b: Show title for record #2 for page content type, one command
  USE node.page;GOTO 2;BROWSE title,nid

Example 2a: Get title from page content type
  SELECT title FROM node.page WHERE status=1 AND (nid=1 OR nid=2) LIMIT 1
  SELECT title FROM node WHERE title LIKE foo%
  SET type WITH article, test WITH 5
  SELECT title FROM node WHERE type=@type

  '*' for fields does not support.

Example 2b: Get all node
  FROM node

Example 3: Combined Fox commands and query
  USE node.page
  WHERE status=0
  SELECT title, nid

Example 4: Replace and append values
  USE node.page;GOTO top
  REPLACE title WITH [{"value":"This is a changed title"}]
  REPLACE title WITH 'Another title'
  APPEND title WITH [{"value":"This is a new title"}]

Example 5: List entities and entity(bundle) fields.
  INFO
  INFO node
  INFO node page

## Table of contents

- Requirements
- Installation
- Configuration
- Maintainers

## Requirements

  - Install drush/drush
  - Install greenlion/php-sql-parser

## Installation

Install as you would normally install a contributed Drupal module. For further information, see [Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).

## Configuration

  Drush command:
    fox:console (fox) --mode=[default]|debug --input="commands split ;" --output
    Run fox commands and query.

## Maintainers

- Serhii Klietsov - [goodboy](https://drupal.org/user/222910)
